// var obj = {
//   name: 'Lekan'
// };
//
// var stringObj = JSON.stringify(obj) ///convert object to string
// console.log(typeof stringObj);
// console.log(stringObj);
//
// var personStr = '{"name": "Lekan", "age": 36}';  //JSON object to be converted to javascript object
// var person = JSON.parse(personStr);
// console.log(typeof person);
// console.log(person);

const fs = require('fs');

var originalNote = {
  title: 'Some title',
  body: 'Some body kinda'
};

var originalNoteString = JSON.stringify(originalNote)
fs.writeFileSync('notes.json', originalNoteString)  //creating a file with the string create above

var noteString = fs.readFileSync('notes.json')
var note = JSON.parse(noteString)
console.log(typeof note);
console.log(note);
