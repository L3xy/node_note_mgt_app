const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

let args = yargs.argv
console.log('Yargs output: ', args)

let cmd = yargs.argv._[0]; //the first command option for yargs


if(cmd === 'add'){
  var note = notes.addNote(args.title, args.content)
  if(note){
    console.log(`Note with Title: ${note.title} & Content: ${note.content} created`)
  } else {
    console.log('No note created')
  }
} else if (cmd === 'list'){
  var allNotes = notes.getAll()
  console.log(`Printing ${allNotes.length} note(s)`)
  allNotes.forEach((note) => {
    console.log(`Title: ${note.title} **Content: ${note.content}`)
  });

} else if (cmd === 'read'){
  var rdNote = notes.readNote(args.title)
  if(rdNote ){
    console.log(`Note with Content: ${rdNote.title} ==> Opened!`)
  } else {
    console.log('No note opened')
  }
} else if (cmd === 'remove'){
  rmdNote = notes.deleteNote(args.title)
  var msg = rmdNote ? 'NO Note removed' : 'One note removed';
  console.log(msg);
} else{
  console.log('No command added')
}
