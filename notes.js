const fs = require('fs');

var fetchNotes = () => {
  try {
    var noteString = fs.readFileSync('note-data.json');
    return JSON.parse(noteString);    //pulling the existing notes
  } catch(e){
    return []
  }
}

var saveNotes = (allNotes) => {
  fs.writeFileSync('note-data.json', JSON.stringify(allNotes))
}

var addNote = (title, content) => {
  debugger;
  var allNotes = fetchNotes();    //the fucntion returns array with data or empty
  var note = {
    title,
    content
  };

  var duplicateTitle = allNotes.filter((note) => note.title === title); //filter generates a new array based on callback function
  if(!duplicateTitle.length){
    allNotes.push(note);  // add single note to array notes
    saveNotes(allNotes);
    return note;
  }else {
    console.log('A note with that title already exits, so I can add')
  }
};

var getAll = () => {
  return fetchNotes();
}

var readNote = (title) => {
  var allNotes = fetchNotes();
  var myNote = allNotes.filter((note) => note.title === title)
  return myNote[0];   //[0] picked the firest one cos array is returned
}

var deleteNote = (title) => {
  var allNotes = fetchNotes()
  var filteredNote = allNotes.filter((note) => note.title !== title)
  saveNotes(filteredNote)
  return allNotes.length === filteredNote.length
}

module.exports = {
  addNote,
  getAll,
  readNote,
  deleteNote
}
